import os
import json

# OpenWeatherMap API Key
weather_api_key = "YOUR KEY HERE!"

# Google API Key
g_key = "YOUR KEY HERE!"

with open(os.path.expanduser(os.path.join('~', '.api_keys.json'))) as keys_fd:
    keys_obj = json.load(keys_fd)
    weather_api_key = keys_obj['weather']['key']
    g_key = keys_obj['google']['key']